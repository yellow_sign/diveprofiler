-include .makerc

checks:
ifeq ("$(wildcard ./.makerc)", "")
		@echo -e "\n\e[31m .makerc IS NOT FOUND! Please make it happen! \e[39m\n"
		@exit 1
endif

clean:
		sudo docker image rm -f ${IMAGE_NAME} || true

stop:
		sudo docker rm -f ${CONTAINER_NAME} || true

build:
		sudo docker build -t ${IMAGE_FULL} .

run:
		sudo docker run -d --restart always --volume $(ROOT_DIR)/stat/static:/app/static --volume $(ROOT_DIR)/stat/templates:/app/app/templates --hostname=${CONTAINER_NAME} --network=${CONTAINER_NETWORK} --network-alias=${CONTAINER_NAME} --name ${CONTAINER_NAME} ${IMAGE_FULL}

run-prod:
		sudo docker run -d --restart always --volume $(ROOT_DIR)/stat/static:/app/static --volume $(ROOT_DIR)/stat/templates:/app/app/templates --hostname=${CONTAINER_NAME} --network=${CONTAINER_NETWORK} --network-alias=${CONTAINER_NAME} --name ${CONTAINER_NAME} ${IMAGE_FULL}

shell:
		sudo docker exec -it ${CONTAINER_NAME} bash

inspect:
		sudo docker inspect ${CONTAINER_NAME}

ip:
		sudo sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CONTAINER_NAME}

all: checks stop build run ip

prod: checks stop build run-prod
