FROM tiangolo/uwsgi-nginx-flask:python3.7

# RUN pip install flask-socketio

RUN apt-get update
RUN apt-get install ffmpeg -yy

RUN pip install dataclasses
RUN pip install Flask-session
RUN pip install pickledb
RUN pip install defusedxml

RUN mkdir -p /tmp/uploads
RUN mkdir -p /tmp/generated

COPY app /app
RUN mkdir -p /var/www/ssl

RUN pip install -r /app/requirements.txt