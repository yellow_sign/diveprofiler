$(document).ready(function () {
    drawPreview();
    $('#fontselect').fontselect({
        searchable: false,
        placeholder: 'Select a font',

    });
    setInterval(get_fb, 5000);

    function get_fb() {
        var feedback = $.ajax({
            type: 'GET',
            dataType: "json",
            url: "/jobs",
            success: function (result) {
                if (result.length > 0){
                    
                    var output = '<tr>';
                    for (var i in result) {
                        if (result[i].status == "Processing" || result[i].status == "Added") {
                            output += '<td class="text-center"><i class="far fa-clock"></i></td>'
                        } else if (result[i].status == "Done"){
                            output += '<td class="bg-success text-white text-center"><i class="fas fa-check"></i></td>'
                        }else {
                            output += '<td class="bg-danger text-white text-center"><i class="fas fa-exclamation-triangle"></i></td>'
                        }
                        output += '<td>'+ result[i].basename + '</td>';
                        if (result[i].status == "Done") {
                            output += '<td><a href="/uploads/' +
                                result[i].generated_name + '">' + result[i].generated_name + '</a></td>';
                        } else {
                            output += '<td>'+result[i].status+'</td>';
                        }
                        output += "</tr>";
                    }
                    $('#jobs').html(output);
                }

            }
        })

    }

    $('#file').change(function (e) {
        var fileName = e.target.files[0].name;
        $("#file-label").text(fileName);
    }); 

    $('#upload').submit(function (event) {
        event.preventDefault();
        if ($('#file').get(0).files.length === 0) {
            $('#upload').addClass("was-validated")
           return;
        }

        var formData = new FormData(this);

        $.ajax({
            url: "/",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,

        });
        get_fb();
        $('#file').val(null);
        $("#file-label").text("Choose file");
        $('#upload').removeClass("was-validated")

    })

    $("#metric").change(function () {
        if (this.checked) {
            $('#metriclabel').text('Imperial (selected)');
        }else{
            $('#metriclabel').text('Metric (default)');

        }
    });


    function drawPreview(){
        canvas = document.getElementById("preview");
        ctx = canvas.getContext("2d");
        align = $('#align').val()
        ctx.textAlign = align
        fontsize = $('#font-size').val()
        width = fontsize * 6
        height = fontsize * 6
        ctx.clearRect(0,0, width+200, height+200);
        // font = $('#fontselect').val().replace(/\+/g, ' ');
       
        // ctx.font = fontsize+"px " + font;
        ctx.font = fontsize+"px " + "Arial";
        ctx.fillStyle = '#'+$('#text-color').val();
        $('#fontSizeHelp').text("Font size: " + fontsize);
        depthText = "5.2 m"
        tempText = "26.2 C"
        timeText = "13:22:06"
        dateText = "2019-12-06"
        lineheight = fontsize;
        elements = [depthText, tempText, timeText, dateText]

        l = 10
        t = 30

        if(align === 'right'){
            l = width
        }else if (align === 'center'){
            l = width
        }


        for (var j = 0; j < elements.length; j++){
            ctx.fillText(elements[j], l,t + (j * lineheight));
        }
        
        $('#preview').css("width", width+'px')
        $('#preview').css("height", height+'px')
        $('#preview').css("background-color", '#' + $('#background-color').val())
    }

    $("#background-color").change(function () {
        drawPreview();
    });

    $("#text-color").change(function () {
        drawPreview();
    });

    $('#fontselect').change(function () {
        drawPreview();
    });


    $('#font-size').change(function () {
        drawPreview();
    });

    $('#leftalign').click(function () {
        $('#leftalign').addClass('selected');
        $('#rightalign').removeClass('selected');
        $('#centeralign').removeClass('selected');
        $('#align').val('left')
        $('#alignmentHelp').text('Text align: left')
        drawPreview();
    });
    $('#rightalign').click(function () {
        $('#leftalign').removeClass('selected');
        $('#rightalign').addClass('selected');
        $('#centeralign').removeClass('selected');
        $('#align').val('right')
        $('#alignmentHelp').text('Text align: right')
        drawPreview();
    });
    $('#centeralign').click(function () {
        $('#leftalign').removeClass('selected');
        $('#rightalign').removeClass('selected');
        $('#centeralign').addClass('selected');
        $('#align').val('center')
        $('#alignmentHelp').text('Text align: center')
        drawPreview();
    });

});