import datetime
from app.DataPoint import DataPoint
import csv
from app.Helper import mk_float, mk_int
from defusedxml.ElementTree import parse


def parse_subsurface_xml(file):
    datapoints = []
    et = parse(file)
    root = et.getroot()
    dives = root.find('dives')
    dive = dives.find('dive')
    divecomputer = dive.find('divecomputer')
    dive_no = dive.get('number')
    date = dive.get('date')
    time = dive.get('time')
    samples = divecomputer.findall('sample')
    year, month, day = map(int, date.split('-'))
    hours, minutes, seconds = map(int, time.split(':'))
    temp_unit = 'C'
    depth_unit = 'm'
    pressure_unit = 'bar'
    for sample in samples:
        sample_minutes, sample_seconds = map(int, sample.get('time').split(' ')[0].split(':'))
        sample_seconds = (sample_minutes*60)+sample_seconds
        actual_time = datetime.datetime(year, month, day, hours, minutes, seconds) + datetime.timedelta(0, sample_seconds)
        
        sample_depth=mk_float(sample.get('depth').split(' ')[0])

        if 'ft' in sample.get('depth'):
            sample_depth = sample_depth / 3.281
        
        try:
            temp=mk_float(sample.get('temp').split(' ')[0])
            if 'F' in sample.get('temp'):
                sample_temp = (temp - 32) * (5/9)
            else:
                sample_temp = temp
        except:
            sample_temp = 0.0
        
        sample_pressure=0
        sample_heartrate=0

        data_point=DataPoint(dive_no, date, time, sample_seconds, sample_depth, sample_temp, sample_pressure, sample_heartrate, actual_time, depth_unit, temp_unit, pressure_unit)
        datapoints.append(data_point)
    
    return datapoints
        
        


def parse_UDDF(file):

    datapoints = []
    et = parse(file)
    root = et.getroot()
    date,time = root[5][0][0][0].find(
        '{http://www.streit.cc/uddf/3.2/}datetime').text.split('T')
    actual_datetime = datetime.datetime.strptime(root[5][0][0][0].find(
        '{http://www.streit.cc/uddf/3.2/}datetime').text, '%Y-%m-%dT%H:%M:%S')
    samples = root[5][0][0][2]

    dive_no = int(root[5][0][0][0].find(
        '{http://www.streit.cc/uddf/3.2/}divenumber').text)

    for sample in samples:
        try:
            sample_depth = mk_float('.'.join(sample.find('{http://www.streit.cc/uddf/3.2/}depth').text.split(',')))
        except:
            sample_depth = 0.0
        
        sample_seconds = mk_int(sample.find('{http://www.streit.cc/uddf/3.2/}divetime').text)
        
        try:
            sample_temp = mk_float(sample.find(
                '{http://www.streit.cc/uddf/3.2/}temperature').text) - 273.15
            print(sample_temp)
        except:
            sample_temp = 0.0
        
        sample_pressure=None
        sample_heartrate=None

        actual_time =  actual_datetime + datetime.timedelta(0, sample_seconds)

        datapoint=DataPoint(dive_no, date, time,
        sample_seconds, sample_depth, sample_temp, sample_pressure, sample_heartrate, actual_time)
        datapoints.append(datapoint)
    return datapoints

def parse_csv(file):
    data_points = []
    temp_unit = 'C'
    depth_unit = 'm'
    pressure_unit = 'bar'
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                if "ft" in row[4]:
                    temp_unit = 'F'
                    depth_unit = 'ft'
                line_count += 1
            else:
                sample_minutes, sample_seconds = map(int, row[3].split(':'))
                sample_seconds = (sample_minutes*60)+sample_seconds
                year, month, day = map(int, row[1].split('-'))
                hours, minutes, seconds = map(int, row[2].split(':'))
                actual_time = datetime.datetime(
                    year, month, day, hours, minutes, seconds) + datetime.timedelta(0, sample_seconds)

                dive_no = mk_int(row[0])
                date = row[1]
                time = row[2]
                sample_depth = mk_float(row[4])
                sample_temp = mk_float(row[5])
                sample_pressure = mk_int(row[6])
                sample_heartrate = mk_int(row[7])
                data_point = DataPoint(dive_no, date, time, sample_seconds, sample_depth, sample_temp, sample_pressure, sample_heartrate, actual_time, depth_unit, temp_unit, pressure_unit)
                data_points.append(data_point)
                line_count += 1
        print(f'Processed {line_count} lines.')
    return data_points


if __name__ == "__main__":
    parse_subsurface_xml("/home/marko/Downloads/dive_subsurface.xml")
