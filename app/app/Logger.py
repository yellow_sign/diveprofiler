from datetime import datetime

class Logger:

    def log(self, file, message):
        now = datetime.now()
        with open(self.logfile,'a+') as logfile:
            logfile.write(f"[{now}][{file}] {message}\n")

    def __init__(self, logfile):
        self.logfile = logfile
