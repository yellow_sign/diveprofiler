import os
from flask import Flask, flash, request, redirect, url_for, render_template, send_from_directory, session, jsonify
from werkzeug.utils import secure_filename
from werkzeug.datastructures import ImmutableMultiDict
from flask_session import Session
import hashlib
from app.app_alt import main as genvid
from threading import Thread
import pickledb


UPLOAD_FOLDER = '/tmp/uploads'
DOWNLOAD_FOLDER = '/tmp/generated/'
ALLOWED_EXTENSIONS = ['csv','uddf','xml']

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
app.config['SESSION_TYPE'] = 'filesystem'
sess = Session(app)
allowed_chars= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():

    width=200
    height=200
    font_size=32

    alignments = ['left','right','center']

    align = 'right'

    if 'files' not in session.keys():
        session['files'] = []
    
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        filename = secure_filename(file.filename)
        basename = '_'.join(os.path.basename(filename).split('.')[0:-1])
        generated_name = "{}.mp4".format(basename)
        h = hashlib.md5(generated_name.encode()).hexdigest()
        session['files'].append(h)

        db = pickledb.load('example.db', True, False)

        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            data = dict(request.form)

            try:
                textcolor = tuple(
                    int(data['text-color'][i:i+2], 16) for i in (0, 2, 4))
            except:
                textcolor = (255,255,255)
            
            try:
                backgroundcolor = tuple(
                    int(data['background-color'][i:i+2], 16) for i in (0, 2, 4))
            except:
                backgroundcolor = (1,1,1)

            try:
                inputwidth = int(data['width'])
                if inputwidth <201 and inputwidth >0:
                    width = inputwidth
            except:
                pass

            try:
                inputheight = int(data['height'])
                if inputheight < 201 and inputheight > 0:
                    height = inputheight
            except:
                pass

            try:
                inputsize = int(data['font-size'])
                if inputsize <= 32 and inputsize > 1:
                    font_size = inputsize
            except:
                pass

            try:
                if data['align'] in alignments:
                    align = data['align']
            except:
                pass

                
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            db.set(h, {'status': 'added', 'basename':filename, 'generated_name':generated_name})

            genvid(os.path.join(app.config['UPLOAD_FOLDER'], filename), h, textcolor, backgroundcolor, width, height, font_size, align)

            print(session['files'])
        else:
            db.set(h, {'status': 'Invalid file extension', 'basename': filename,
                       'generated_name': filename})



    return render_template('index.html')


@app.route('/uploads/<generated_name>')
def download(generated_name):
    return send_from_directory(directory=app.config['DOWNLOAD_FOLDER'], filename=generated_name, as_attachment=True)


@app.route('/jobs')
def jobs():
    if 'files' not in session.keys():
        session['files'] = []

    resp = []

    db = pickledb.load('example.db', False)

    for h in session['files']:
        resp.append(db.get(h))

    return jsonify(resp)

if __name__ == "__main__":
    app.secret_key = 'super secret key'
    sess.init_app(app)

    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
