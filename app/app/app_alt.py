import moviepy.editor as mpy
from math import pi
from sys import argv
import datetime
import os
import threading
from operator import itemgetter
from PIL import Image, ImageDraw, ImageFont
from uwsgidecorators import thread
import pickledb
from app.DataPoint import DataPoint
from app.parse_csv import parse_csv, parse_UDDF, parse_subsurface_xml
from app.Logger import Logger

txt_values = []
video_values = {}
counter = 0
total = 0
temp_unit = 'C'
depth_unit = 'm'
logger = Logger('/var/log/diveprofiler.log')

def clear(): return os.system('clear')

def impute(start, stop, size):

    final_list = []

    delta = stop-start

    step = delta/(size+1)

    for n in range(size):
        final_list.append(start + (step*(n+1)))

    return final_list

def parse_profile(file):
    filename, file_extension = os.path.splitext(file)

    logger.log(file, "Starting to parse")
    if file_extension.lower() == '.csv':
        logger.log(file, "Discovered csv file")
        data_points = parse_csv(file)
    elif file_extension.lower() == '.uddf':
        logger.log(file, "Discovered uddf file")
        data_points = parse_UDDF(file)
    elif file_extension.lower() == '.xml':
        logger.log(file, "Discovered xml file")
        data_points = parse_subsurface_xml(file)
    return data_points


def data_points_to_video(datapoint1, datapoint2):

    start_time = datapoint1.actual_time + \
        datetime.timedelta(0, datapoint1.sample_time)
    start_depth = datapoint1.sample_depth

    end_time = datapoint1.actual_time + \
        datetime.timedelta(0, datapoint2.sample_time)

    end_depth = datapoint2.sample_depth
    delta_times = (end_time-start_time).seconds

    new_points = []

    imputed = impute(start_depth, end_depth, delta_times-1)

    for n in range(1, delta_times):

        actual_time = datapoint1.actual_time + datetime.timedelta(0, n)

        calc = imputed[n-1]
        calc_float = float("{:.1f}".format(calc))

        new_datapoint = DataPoint(datapoint2.dive_no, datapoint2.date, datapoint2.time,
                                  datapoint1.sample_time+n, calc_float, 0.0, 0, 0, actual_time,)

        new_points.append(new_datapoint)

    return new_points


def get_dict_first_key(input_dict):
    return list(input_dict.keys())[0]


def get_all_temperatures(datapoints):
    returned_points = []
    for n in range(1, len(datapoints)):
        current_datapoint = datapoints[n]
        if not current_datapoint.sample_temp:
            pass
        else:
            returned_points.append({n: current_datapoint.sample_temp})
    return returned_points

def chunks(xs, n):
    '''Split the list, xs, into n chunks'''
    L = len(xs)
    assert 0 < n <= L
    s, r = divmod(L, n)
    chunks = [xs[p:p+s] for p in range(0, L, s)]
    chunks[n-1:] = [xs[-r-s:]]
    return chunks


def set_file_status(h, status):
    db = pickledb.load('example.db', True, False)
    val = db.get(h)
    val['status'] = status
    db.set(h, val)

@thread
def main(divefile, h="", textcolor=(255, 255, 255), backgroundcolor=(1, 1, 1), width=200, height=200, fontsize=32, align='right', dateformat='%Y-%m-%d', timeformat='%H:%M:%S', standalone=False):

    start_time = datetime.datetime.now()

    final_datapoints = []

    dive_profile = divefile

    try:
        datapoints = parse_profile(dive_profile)
    except Exception as e:
        if not standalone:
            set_file_status(h, "Unable to parse CSV file")
            logger.log(divefile, e)
            print(e)
        return

    logger.log(divefile, f"{len(datapoints)} data points retrieved")

    first_datapoint = DataPoint(
        datapoints[0].dive_no, datapoints[0].date, datapoints[0].time, 0.0, 0, datapoints[0].sample_temp, datapoints[0].sample_pressure, datapoints[0].sample_heartrate, datapoints[0].actual_time-datetime.timedelta(0, 5))

    datapoints.insert(0, first_datapoint)

    if not standalone:
        set_file_status(h, 'Processing')

    for n in range(len(datapoints)):
        final_datapoints.append(datapoints[n])

        if n != len(datapoints)-1:
            generated = data_points_to_video(datapoints[n], datapoints[n+1])

            [final_datapoints.append(g) for g in generated]

    total = len(final_datapoints)
    logger.log(divefile, f"{total} total datapoints")

    temps_found = get_all_temperatures(final_datapoints)

    logger.log(divefile, f"Found {len(temps_found)} temperatures from all datapoints")

    first_temp_pos = get_dict_first_key(temps_found[0])

    for n in range(first_temp_pos):
        final_datapoints[n].sample_temp = temps_found[0][first_temp_pos]

    last_temp_pos = get_dict_first_key(temps_found[-1])

    for n in range(last_temp_pos+1, len(final_datapoints)):
        final_datapoints[n].sample_temp = temps_found[-1][last_temp_pos]

    for n in range(len(temps_found)-1):
        datapoint_a = temps_found[n]
        datapoint_b = temps_found[n+1]

        (begin_pos, begin_temp), = datapoint_a.items()
        (end_pos, end_temp), = datapoint_b.items()

        positions_to_fill = [n for n in range(begin_pos+1, end_pos)]

        imputed = impute(begin_temp, end_temp, len(positions_to_fill))

        for n in range(len(positions_to_fill)):
            final_datapoints[positions_to_fill[n]].sample_temp = imputed[n]


    print ("Units before creating files")
    print(depth_unit)
    print(temp_unit)
    for n in range(len(final_datapoints)):
        temp = "{:.1f}".format(final_datapoints[n].sample_temp)
        depth = "{:.1f}".format(final_datapoints[n].sample_depth)
        date= final_datapoints[n].actual_time.strftime(dateformat)
        time = final_datapoints[n].actual_time.strftime(timeformat)
       
        img = Image.new('RGB', (width, height), color=backgroundcolor)
        d = ImageDraw.Draw(img)

        fnt = ImageFont.truetype(
            '/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf', fontsize)
        d.multiline_text((10, 10), f"{depth} {depth_unit}\n{temp} {temp_unit}\n{time}\n{date}",font=fnt, fill=textcolor, align=align)


        basename = '_'.join(os.path.basename(dive_profile).split('.')[0:-1])

        os.system(f'mkdir -p /tmp/{basename}')
        img.save(f'/tmp/{basename}/{n}.png')

    os.system(
        f'ffmpeg -an -f image2 -framerate 1 -pattern_type sequence -start_number 0 -r 1 -i /tmp/{basename}/%d.png -s {width}x{height} /tmp/generated/{basename}.mp4 -y')

    os.system(f'rm -rf /tmp/{basename}')

    end_time = datetime.datetime.now()

    if not standalone:
        set_file_status(h, "Done")

    print(end_time-start_time)

if __name__ == '__main__':
    main(argv[1])

