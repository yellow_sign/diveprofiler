from dataclasses import dataclass

@dataclass
class DataPoint:
    dive_no: int
    date: str
    time: str
    sample_time: int
    sample_depth: float
    sample_temp: float
    sample_pressure: int
    sample_heartrate: int
    actual_time: str
    temp_unit: str = 'C'
    depth_unit: str = 'm'
    pressure_unit: str = 'bar'
