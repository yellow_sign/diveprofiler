def mk_int(s):
    s = s.strip()
    return int(s) if s else 0


def mk_float(s):
    s = s.strip()
    return float(s) if s else 0.0
